package life.maxima.java;

public class LoopAndConditionals {

    public static void main(String[] args) {
        int a = 3;

        if (true) {
            System.out.println("!!!");
        }

        if (false) {
            System.out.println("???");
            System.out.println("***");
        }

        if (a == 1) {
            System.out.println("a is one");
        } else if (a == 2) {
            System.out.println("a is two");
        } else if (a == 3) {
            System.out.println("a is three");
        } else if (a >= 4 && a <= 5) {
            System.out.println("a is from four to five");
        } else {
            System.out.println("a is other");
        }

        System.out.println(a == 1 ? "a is one" : "a is not one");

        switch (a){
            case 1:
                System.out.println("a is one");
                break;
            case 2:
                System.out.println("a is two");
                break;
            case 3,4,5:
                System.out.println("a is from three to five");
                break;
            default:
                System.out.println("a is other");
        }

        switch (a){
            case 1 -> System.out.println("a is one");
            case 2 -> System.out.println("a is two");
            default -> System.out.println("a is other");
        }

        System.out.println("---");
        for (int i = 1;i <= 10;i++){
            System.out.println("Hello " + i);
        }

        System.out.println("---");
        int number = 1;

        while (number <= 10){
            System.out.println(number++);
        }
        System.out.println("---");

        long t1 = System.currentTimeMillis();

        int count = 0;
        while (System.currentTimeMillis() - t1 < 10){
            System.out.println(++count);
            if (count >= 400)
                break;
        }
        System.out.println("---");

        do {
            System.out.println("Hello!");
        }while (false);
        System.out.println("---");

        int sum = 0;
        for (int i = 1; i <= 10; i++) {
            if (i % 2 != 0)
                continue;
            System.out.println(i);
            sum += i;
        }

        System.out.println("Sum = " + sum);

    }
}
