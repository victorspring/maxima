package life.maxima.java;

import java.util.Arrays;
import java.util.Locale;

public class StringExamples {

    public static void main(String[] args) {
        long a = 10L;
        char c = 'q';
        char[] charArray = new char[10];

        System.out.println(1 == 1);

        String s1 = "Hello";
        String s2 = "Hello";

        System.out.println(s1 == s2);

        int[] a1 = {1,2,3};
        int[] a2 = {1,2,3};
        System.out.println(a1 == a2);

        Integer i1 = 127;
        Integer i2 = 127;
        System.out.println(i1 == i2);
        System.out.println("---");

        String hello = "Hello";
        System.out.println(hello.toUpperCase());
        System.out.println(hello.substring(1, 3));
        System.out.println("Hello".isEmpty());
        System.out.println("".length());
        System.out.println("".isEmpty());
        System.out.println("       ".isBlank());
        System.out.println("Hello".indexOf('l'));
        System.out.println("Hello".lastIndexOf('l'));
        System.out.println(Arrays.toString("Hello".toCharArray()));
        System.out.println(Arrays.toString("Hello".split("l")));

        String result = "Hel" + "lo";
        System.out.println(result);
        System.out.println("---");

        StringBuilder str = new StringBuilder();
        for (int i = 1; i <= 5; i++) {
            str.append(i);
            str.append(", ");
        }

        str.delete(str.length() - 2, str.length());
        System.out.println(str);
        System.out.println("---");

        String newString = null;
        char[] charArray2 = null;

        System.out.println(newString != null ? newString.toUpperCase() : "NULL");



    }
}
