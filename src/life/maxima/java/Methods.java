package life.maxima.java;

import java.util.Arrays;

public class Methods {

    public static void main(String[] args) {
        System.out.println(hello("Victor"));
        System.out.println(hello("Admin", "Guest"));
        System.out.println(hello("Admin", "Guest", "User"));
        System.out.println(hello());
//        sof();
    }

    public static String hello(String name){
        return "Hello, " + name + "!";
    }

    public static String hello(String name1, String name2){
        return hello(name1 + ", " +name2);
    }

    public static String hello(String... names){
        return hello(String.join(", ", names));
    }

    public static String hello(){
        return hello("user");
    }

    public static void sof(){
        sof();
    }


}
