package life.maxima.java;

public class GCD {

    public static void main(String[] args) {
        int n = 106;
        int m = 16;

//        System.out.println(gcdLoop(n,m));
        System.out.println(gcdRecursion(n,m));

    }

    public static int gcdLoop(int a, int b){
        while (b != 0) {
            int r = a % b;
            a = b;
            b = r;
        }

        return a;
    }

    public static int gcdRecursion(int a, int b){
        return a % b == 0
                ? b
                : gcdRecursion(b, a % b);
    }
}
