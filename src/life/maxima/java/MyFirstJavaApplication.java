package life.maxima.java;

/**
 * First Java Application
 */
public class MyFirstJavaApplication {

    //Line comment

    /*
        Block
        Comment
     */

    /**
     * Main method of the first Java application
     * @param args input parameters
     */
    public static void main(String[] args){
        System.out.println("Hello world!!!");
        System.out.println(237);

        int myNumber;
        myNumber = 9;
        System.out.println(myNumber);

        myNumber = 1010;
        System.out.println(myNumber);

        byte myByte = 127;
        System.out.println(myByte);

        myByte = 127;
//        myByte = myByte + 1;
        System.out.println(myByte);

        int number = Integer.MAX_VALUE;
        System.out.println(number);

        number = Integer.MAX_VALUE + 1;
        System.out.println(number);

        double myDouble = 9.9235235334634659;
        System.out.println(myDouble);

        char myChar = '\n';

        boolean myBoolean = false;

        System.out.println("Hello");
        System.out.println('\n' + "Hello");
        System.out.println('\t' + "Hello");
        System.out.println('\u005A');

        char zChar1 = '\u005A';
        char zChar2 = 'Z';
        System.out.println(zChar1 == zChar2);

        int one = 1;
        System.out.println(one == 1);
        System.out.println(1 == 2);
        System.out.println(1 != 2);
        System.out.println(false == true);

        System.out.println(1 < 2);
        System.out.println(2 <= 2);
        System.out.println(2 < 2.1);

        System.out.println(1 + 1);
        System.out.println(5 - 3);
        System.out.println(5 * 5);
        System.out.println(12 % 5);
        System.out.println(5 / 2);
        System.out.println(5.0 / 2);
        System.out.println(5d / 2);

        double five = 5d;
        System.out.println(5 / (double)2);
        System.out.println("---");

        long l = 10;
        int i = 345345349;

        long l1 = i;

        long l2 = 10;
        int i1 = (int)l2;

        System.out.println(i1);

        int result = (1 + 1) * 2;
        System.out.println(result);
        System.out.println("---");

        int n = 1;
        n = n + 1;

        System.out.println(n);

        n += 1;
        System.out.println(n);

        n++; // n += 1;
        System.out.println(n);

        ++n;
        System.out.println(n);
        System.out.println("---");
        int num1 = 0;
        int num2 = 0;

        System.out.println(++num1);
        System.out.println(num2++);
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);

        int num3 = 1;
        System.out.println(num3++ + ++num3); //1 + 3
        System.out.println("---");

        System.out.println(!true);
        System.out.println(!(1 > 2));
        System.out.println("---");

        System.out.println(true && true);
        System.out.println(false && true);
        System.out.println(true && false);
        System.out.println(false && false);
        System.out.println("---");

        System.out.println(true || true);
        System.out.println(false || true);
        System.out.println(true || false);
        System.out.println(false || false);

    }


}
