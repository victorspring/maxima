package life.maxima.java;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ArraysExamples {

    public static void main(String[] args) {
        int[] array = new int[10];
        System.out.println(array);
        System.out.println(Arrays.toString(array));

        int[] array1 = {1, -10, 999};
        System.out.println(Arrays.toString(array1));

        array1 = new int[]{37,235,12,67,3};
//        37,235,12,67,3
//         0   1  2  3 4

        System.out.println(Arrays.toString(array1));

        System.out.println(array1.length);
        System.out.println(array1[0]);
        System.out.println(array1[1]);
        System.out.println(array1[array1.length - 1]);
        array1[array1.length - 1] = -9;
//        System.out.println(array1[-1]);

        System.out.println("---");
        for (int i = 0; i < array1.length; i++) {
            System.out.println(array1[i] + ", i = " + i);
        }

        System.out.println("---");

        for (int number : array1) {
            System.out.println(number);
        }

        int[][] multiArray = {array, array1};
        System.out.println(Arrays.deepToString(multiArray));

        int[][] matrix = {{1,2,3}, {4,5,6}, {7,8,9}};
        System.out.println(Arrays.deepToString(matrix));

        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("---");

        System.out.println(Arrays.toString(array1));
        Arrays.sort(array1);

        System.out.println(Arrays.toString(array1));

        int[] newArray = Arrays.copyOf(array1, 3);
        System.out.println(Arrays.toString(newArray));
        System.out.println("---");

        ArrayList<Integer> list = new ArrayList();

        Integer i = 0;
        list.add(1);
        list.add(2);
        list.add(3);
//        list.add("Hello");

        System.out.println(list);
        System.out.println(list.get(1));
        System.out.println(list.size());


    }
}
