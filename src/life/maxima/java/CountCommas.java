package life.maxima.java;

import java.util.Scanner;

public class CountCommas {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();

//        System.out.println(countCommas(str));
        System.out.println(countCommasAlt(str));
    }


    public static int countCommas(String str){
        int counter = 0;

        for (char c : str.toCharArray()){
            if (c == ','){
                counter++;
            }
        }

        return counter;
    }

    public static int countCommasAlt(String str){
        return str.split(",").length - 1;
    }


}
