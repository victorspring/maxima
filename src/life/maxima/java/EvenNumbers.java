package life.maxima.java;

import java.util.ArrayList;
import java.util.Arrays;

public class EvenNumbers {


    public static void main(String[] args) {
        int[] array = {222, 9, -3, 20, 1, 4, 5, 6, 100};
//        System.out.println(evenCount(array));
//        System.out.println(Arrays.toString(evenArray(array)));
        System.out.println(evenList(array));
    }

    public static int evenCount(int[] array){
        int evenNumbersCounter = 0;
        for (int number : array) {
            if (number % 2 == 0) {
                evenNumbersCounter++;
            }
        }

        return evenNumbersCounter;
    }

    public static int[] evenArray(int[] array){
        int[] newArray = new int[array.length];
        int index = 0;

        for (int number : array) {
            if (number % 2 == 0) {
                newArray[index++] = number;
            }
        }

        return Arrays.copyOf(newArray, index);
    }

    public static ArrayList<Integer> evenList(int[] array){
        ArrayList<Integer> result = new ArrayList<>();
        for (int number : array) {
            if (number % 2 == 0) {
                result.add(number);
            }
        }

        return result;
    }





}
