package life.maxima.java;

import java.util.Scanner;

public class TextInput {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your name?");
        String name = scanner.nextLine();

        System.out.println("Enter your age?");
        int age = Integer.parseInt(scanner.nextLine());

        System.out.println(name + ": " + age);

    }

}
