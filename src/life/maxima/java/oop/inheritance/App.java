package life.maxima.java.oop.inheritance;

import life.maxima.java.oop.Singleton;

import java.util.ArrayList;
import java.util.Arrays;

public class App {

    public static void main(String[] args) throws CloneNotSupportedException {


        Rectangle r = new Rectangle(10.5, 20.5);
        r.setX(10);
        r.setY(15);

        Ellipse e = new Ellipse();
        e.setR1(5);
        e.setR2(10);
        e.setX(10);
        e.setY(15);

        Object o = r;

        Shape s = Math.random() > 0.5 ? e : r;


        System.out.println(s.area());
        System.out.println(s);

        Circle c = new Circle();
        c.setR(10);

        System.out.println(c.area());

//        Square square = new Square();
//        square.setSide(10);
//        System.out.println(square.area());

        Circle circle = new Circle(5, 10, 10);
        System.out.println(circle);

        Shape shape;
        shape = circle;
        System.out.println(shape.area());

        System.out.println("---");

        Rectangle r1 = new Rectangle(10, 15, 0, 0);
        Circle c1 = new Circle(20,  0, 0);

        System.out.println(r1);
        System.out.println(c1);

        Scalable scalable = r1;
        scalable.scale(2);

        scalable = c1;
        scalable.scale(2);

        System.out.println(r1);
        System.out.println(c1);

        System.out.println("---");

        Circle newCircle = new Circle(10, 0, 0);
        Ellipse ellipse = newCircle;

        Rectangle rectangle = new Rectangle(20,40,0,0);
        Object object = rectangle;


        Ellipse newEllipse = Math.random() > 0.5
                ? new Ellipse(20,50,0,0)
                : new Circle(14,0,0);

        if (newEllipse instanceof Circle){
            Circle anotherCircle = (Circle)newEllipse;
            System.out.println("It's a circle");
            System.out.println(anotherCircle);
        } else {
            System.out.println("It's an ellipse");
            System.out.println(newEllipse);
        }
        System.out.println("---");

        System.out.println(circle instanceof Circle);
        System.out.println(circle instanceof Ellipse);
        System.out.println(circle instanceof Shape);
        System.out.println(circle instanceof Object);
        System.out.println(circle instanceof Scalable);


        System.out.println(rectangle instanceof Square);
        System.out.println("Hello" instanceof Object);

        ArrayList<Shape> list = new ArrayList();
        System.out.println("---");

        Circle oneMoreCircle = new Circle(5,5,5);
        oneMoreCircle.scale();
        System.out.println(oneMoreCircle);
        System.out.println("---");

        Rectangle newRectangle = rectangle.clone();
        System.out.println(rectangle);
        System.out.println(newRectangle);

        System.out.println(rectangle == newRectangle);
        System.out.println(rectangle.equals(newRectangle));

        String s1 = "Hello";
        String s2 = new String("Hello");

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));

        Class<? extends Rectangle> rectangleClass = rectangle.getClass();
        System.out.println(rectangleClass.getCanonicalName());
        System.out.println(rectangleClass.getSimpleName());
        System.out.println(Arrays.toString(rectangleClass.getDeclaredFields()));
        System.out.println("---");

        Singleton singleton1 = Singleton.getInstance();
        Singleton singleton2 = Singleton.getInstance();

        System.out.println(singleton1 == singleton2);
    }
}
