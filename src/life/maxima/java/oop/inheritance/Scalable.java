package life.maxima.java.oop.inheritance;

@FunctionalInterface
public interface Scalable {

    int DEFAULT_FACTOR = 2;

    static void doSomething(){
        System.out.println("Hello!!!");
    }

    void scale(double factor);


    default void scale(){
        scale(DEFAULT_FACTOR);
    }


}
