package life.maxima.java.oop.inheritance;

import java.util.ArrayList;

public class HomeworkAbstraction {

    public static void main(String[] args) {
        ArrayList<Shape> list = new ArrayList();
        list.add(new Rectangle(10, 20, 0, 0));
        list.add(new Circle(15, 0, 0));
        list.add(new Point(0, 0));

        for (Shape s : list){
            System.out.println(s + "; area = " + s.area());
        }

        for (Shape s : list){
            if (s instanceof Scalable){
                ((Scalable) s).scale(2);
            }
        }

        for (Shape s : list){
            System.out.println(s + "; area = " + s.area());
        }

        System.out.println(Scalable.DEFAULT_FACTOR);

    }
}
