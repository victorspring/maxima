package life.maxima.java.oop.inheritance;

import java.io.Closeable;

public class Rectangle extends Shape implements Scalable, Cloneable {

    private double width;
    private double height;

    public Rectangle() {
    }

    public Rectangle(double width,
                     double height) {
        setWidth(width);
        setHeight(height);
    }

    public Rectangle(double width,
                     double height,
                     int x,
                     int y) {
        super(x, y);
        setWidth(width);
        setHeight(height);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double area() {
        return width * height;
    }


    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    @Override
    public void scale(double factor) {
        setWidth(getWidth() * factor);
        setHeight(getHeight() * factor);
    }

    @Override
    public Rectangle clone() throws CloneNotSupportedException {
        return (Rectangle) super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Rectangle))
            return false;

        Rectangle rectangle = (Rectangle) obj;
        return getWidth() == rectangle.getWidth() &&
                getHeight() == rectangle.getHeight() &&
                getX() == rectangle.getX() &&
                getY() == rectangle.getY();

    }
}
