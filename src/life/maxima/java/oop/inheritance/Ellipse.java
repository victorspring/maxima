package life.maxima.java.oop.inheritance;

import java.util.Objects;

public class Ellipse extends Shape implements Scalable {

    private double r1;
    private double r2;

    public Ellipse() {
    }

    public Ellipse(double r1,
                   double r2,
                   int x,
                   int y) {
        super(x, y);
        setR1(r1);
        setR2(r2);
    }

    public double getR1() {
        return r1;
    }

    public void setR1(double r1) {
        this.r1 = r1;
    }

    public double getR2() {
        return r2;
    }

    public void setR2(double r2) {
        this.r2 = r2;
    }

    @Override
    public double area(){
        return Math.PI * r1 * r2;
    }


    @Override
    public String toString() {
        return "Ellipse{" +
                "r1=" + r1 +
                ", r2=" + r2 +
                '}';
    }

    @Override
    public void scale(double factor) {
        setR1(getR1()*factor);
        setR2(getR2()*factor);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ellipse ellipse = (Ellipse) o;
        return Double.compare(ellipse.getR1(), getR1()) == 0
                && Double.compare(ellipse.getR2(), getR2()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getR1(), getR2());
    }
}
