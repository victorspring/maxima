package life.maxima.java.oop;

import java.util.ArrayList;
import java.util.Scanner;

public class HomeworkOOP {

    private static final String PROMPT = "Press \"ENTER\" to continue or type \"exit\" to list all humans";
    private static final String ENTER_THE_NAME = "Enter the name?";
    private static final String ENTER_THE_AGE = "Enter the age?";
    public static final String WRONG_NUMBER = "Wrong number";
    public static final String AGE_MUST_BE_POSITIVE = "Age must be positive";

    public static void main(String[] args) {
        ArrayList<Human> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);


        while (!"exit".equalsIgnoreCase(input(scanner, PROMPT))){
            String name = input(scanner, ENTER_THE_NAME);
            int age = -1;
            do {
                try {
                    age = Integer.parseInt(input(scanner, ENTER_THE_AGE));
                } catch (NumberFormatException e){
                    System.out.println(WRONG_NUMBER);
                    continue;
                }
                if (age <= 0){
                    System.out.println(AGE_MUST_BE_POSITIVE);
                }
            } while (age < 0);

            list.add(new Human(name, age));

        }

        for (Human h : list){
            System.out.println(h.getName() + ": " + h.getAge());
        }
    }

    public static String input(Scanner scanner, String prompt){
        System.out.println(prompt);
        return scanner.nextLine();
    }
}
