package life.maxima.java.oop;

public class Human {

    private String name;
    private int age;

    public Human(){

    }


    public Human(String name, int age){
        setName(name);
        setAge(age);
    }

    //getter
    public String getName(){
        return name;
    }

    //setter
    public void setName(String name){
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age <= 0){
            throw new IllegalArgumentException("Age must be positive!");
        }
        this.age = age;
    }
}
