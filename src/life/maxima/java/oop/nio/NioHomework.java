package life.maxima.java.oop.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class NioHomework {

    private static final String IN = "homework_in.txt";
    private static final String OUT = "homework_out.txt";

    public static void main(String[] args) {
        try {
            List<String> list = Files.readAllLines(Paths.get(IN)).stream()
                    .map(Integer::parseInt)
                    .sorted()
                    .map(String::valueOf).toList();
            Files.write(Paths.get(OUT), list);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
}

