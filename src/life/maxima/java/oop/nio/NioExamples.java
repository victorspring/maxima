package life.maxima.java.oop.nio;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class NioExamples {

    public static final String INPUT = "input.txt";
    public static final String OUTPUT = "output.txt";

    public static void main(String[] args) {
        Path path = Paths.get(INPUT);
        Path output = Paths.get(OUTPUT);
        System.out.println(path.getClass().getCanonicalName());

        Path absPath = path.toAbsolutePath();
        System.out.println(absPath);
        System.out.println(absPath.getParent());
        System.out.println(absPath.getRoot());

        try {
            Files.copy(path, output, StandardCopyOption.REPLACE_EXISTING);
            String oneString = Files.readString(path);
            System.out.println(oneString);

            List<String> list = Files.readAllLines(path);
            System.out.println(list);

            Files.writeString(output, System.lineSeparator(), StandardOpenOption.APPEND);
            Files.write(output, list, StandardOpenOption.APPEND);


        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
