package life.maxima.java.oop.io;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class IoHomework2 {

    private static final String IN = "homework_in.txt";
    private static final String OUT = "homework_out.txt";

    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(new FileReader(IN));
             PrintWriter writer = new PrintWriter(new FileWriter(OUT))){
            ArrayList<Integer> list = new ArrayList<>();

            while (scanner.hasNextInt()){
                list.add(scanner.nextInt());
            }

            Collections.sort(list);

            for (Integer i : list){
                writer.println(i);
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
