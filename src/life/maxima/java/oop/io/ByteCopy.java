package life.maxima.java.oop.io;

import java.io.*;

public class ByteCopy {

    public static final String INPUT = "input.txt";
    public static final String OUTPUT = "output.txt";

    public static void main(String[] args) {
        try(InputStream input = new FileInputStream(INPUT);
            OutputStream output = new FileOutputStream(OUTPUT)) {
            int data;
            while ((data = input.read()) != -1) {
                output.write(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
