package life.maxima.java.oop.io;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IoHomework {

    private static final String IN = "homework_in.txt";
    private static final String OUT = "homework_out.txt";

    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new FileReader(IN));
             PrintWriter writer = new PrintWriter(new FileWriter(OUT))){
            String line;
            ArrayList<Integer> list = new ArrayList<>();
            while ((line = reader.readLine()) != null){
                list.add(Integer.parseInt(line));
            }

            Collections.sort(list);

            for (Integer i : list){
                writer.println(i);
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
