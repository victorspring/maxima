package life.maxima.java.oop.io;


import java.io.*;

public class CharacterCopy {

    public static final String INPUT = "input.txt";
    public static final String OUTPUT = "output.txt";

    public static void main(String[] args) {
        try(FileReader input = new FileReader(INPUT);
            FileWriter output = new FileWriter(OUTPUT)) {
            int data;
            while ((data = input.read()) != -1) {
                output.write(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
