package life.maxima.java.oop.io;

import java.io.IOException;

public class MyResource implements AutoCloseable{

    @Override
    public void close() {
        System.out.println("Closing all resources");
    }

}
