package life.maxima.java.oop;

import life.maxima.java.oop.Human;

import java.util.ArrayList;

public class Application {

    public static void main(String[] args) {
//        Class.forName("life.maxima.java.oop.Human");
        Human ivan = new Human();
        ivan.setName("Ivan");
        ivan.setAge(30);

        System.out.println("Name 1: " + ivan.getName());
        System.out.println("Age 1: " + ivan.getAge());

        Human masha = new Human("Masha", 35);

        System.out.println("Name 2: " + masha.getName());
        System.out.println("Age 2: " + masha.getAge());
        System.out.println("---");

        ArrayList<Human> list = new ArrayList<>();
        list.add(ivan);
        list.add(masha);

        for (Human h : list){
            System.out.println(h.getName() + ": " + h.getAge());
        }


    }
}
