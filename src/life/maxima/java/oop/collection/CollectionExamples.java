package life.maxima.java.oop.collection;

import life.maxima.java.oop.inheritance.Circle;

import java.util.*;

public class CollectionExamples {

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);

        Iterable<Integer> iterable = arrayList;

        Iterator<Integer> iterator = iterable.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        Collection<Integer> collection = arrayList;
        System.out.println(collection.size());
        System.out.println(collection.contains(1));
        System.out.println(collection);

        List<Integer> list = arrayList;
        System.out.println(list.indexOf(2));
        System.out.println(list.indexOf(4));
        System.out.println(list.get(2));
//        System.out.println(list.get(4));

        List<Integer> unmodifiableList = List.of(1,2,3);
//        unmodifiableList.add(1);
        System.out.println("---");

        Queue<Integer> queue = new ArrayDeque<>();
        queue.offer(1);
        queue.offer(2);
        queue.offer(3);
        System.out.println(queue);
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue);

        Deque<Integer> deque = new ArrayDeque<>();
        deque.push(1);
        deque.push(2);
        deque.push(3);
        System.out.println(deque);

        System.out.println(deque.peek());
        System.out.println(deque.pop());
        System.out.println(deque.pop());
        System.out.println(deque.pop());

        System.out.println(deque);
        System.out.println("---");


        Map<String, Integer> hashMap = new HashMap<>();
        hashMap.put("John", 236236);
        hashMap.put("Lisa", 9235235);
        hashMap.put("Sam", 9623462);

        Integer number = hashMap.get("Lisa");
        System.out.println(number);

        Map<Circle, String> circleHashMap = new HashMap<>();
        circleHashMap.put(new Circle(1,0,0), "Red");
        circleHashMap.put(new Circle(5,0,0), "Yellow");
        circleHashMap.put(new Circle(10,0,0), "Green");

        System.out.println(circleHashMap.get(new Circle(1,0,0)));

        for (String key : hashMap.keySet()){
            System.out.println(key + " -> " + hashMap.get(key));
        }
        System.out.println("---");

        for (Map.Entry<String, Integer> entry : hashMap.entrySet()){
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }

        Set<String> hashSet = new HashSet<>();
        hashSet.add("John");
        hashSet.add("Lisa");
        hashSet.add("Sam");

        System.out.println(hashSet);
        System.out.println(hashSet.contains("John"));
        System.out.println(hashSet.contains("Ivan"));
        System.out.println("---");

        System.out.println(hashMap);
//       {John=236236, Sam=9623462, Lisa=9235235}

        System.out.println(hashMap.entrySet());
//       {(John,236236), (Sam,9623462), (Lisa,9235235)}

    }
}
