package life.maxima.java.oop.nested;

import life.maxima.java.oop.inheritance.Circle;
import life.maxima.java.oop.inheritance.Rectangle;
import life.maxima.java.oop.inheritance.Square;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.function.*;

public class App {

    public static void main(String[] args) {
        House.Builder builder = new House.Builder();

        House house = builder.setDoors(1)
                .setWalls(4)
                .setWindows(2)
                .build();

        System.out.println(house);

        ArrayList<Circle> circles = new ArrayList<>();
        circles.add(new Circle(30,0,0));
        circles.add(new Circle(10,0,0));
        circles.add(new Circle(20,0,0));
        circles.add(new Circle(50,0,0));
        circles.add(new Circle(40,0,0));

        Collections.sort(circles);

        System.out.println(circles);
        System.out.println("---");

        ArrayList<Square> squares = new ArrayList<>();
        squares.add(new Square(3,0,0));
        squares.add(new Square(5,0,0));
        squares.add(new Square(1,0,0));
        squares.add(new Square(2,0,0));
        squares.add(new Square(7,0,0));
        squares.add(new Square(4,0,0));

        class SquareComparator implements Comparator<Square> {
            @Override
            public int compare(Square s1, Square s2) {
                return (int)(s1.getSide() - s2.getSide());
            }
        }

        Comparator<Square> comparator = new Comparator<Square>() {
            @Override
            public int compare(Square s1, Square s2) {
                return (int)(s1.getSide() - s2.getSide());
            }
        };

//        Collections.sort(squares, (s1, s2) -> (int)(s1.getSide() - s2.getSide()));
        Collections.sort(squares, (o1, o2) -> (int)(o1.area() - o1.area()));

        System.out.println(squares);
        System.out.println("---");

        BiFunction<Integer, Integer, Integer> sum = Integer::sum;
        BiFunction<Integer, Integer, Integer> divide = (i1, i2) -> i1 / i2;

        System.out.println(calc(sum, 10, 5));
        System.out.println(calc((i1, i2) -> i1 - i2, 10, 5));
        System.out.println(calc(new BiFunction<Integer, Integer, Integer>() {
            @Override
            public Integer apply(Integer i1, Integer i2) {
                return i1 * i2;
            }
        }, 10, 5));
        System.out.println(calc(divide, 10, 5));

        IntUnaryOperator square = x -> x*x;

        System.out.println(square.applyAsInt(9));

        Predicate<String> isEmpty = s -> s == null || s.isEmpty();

        System.out.println(isEmpty.test("hello"));
        System.out.println(isEmpty.test(null));

    }

    public static int calc(BiFunction<Integer, Integer, Integer> function, int i1, int i2){
        return function.apply(i1, i2);
    }

}
