package life.maxima.java.oop.nested;

import java.util.Arrays;
import java.util.function.IntPredicate;

public class Sequence {

    //1) Проверка на четность:
    //[123,104,75, 81, 1, 8]  -> [104,8]

    //2) Проверка суммы цифр элемента на четность:
    //[123,104,75, 81, 1, 8] -> [123,75,8]

    public static int[] filter (int[] array, IntPredicate predicate) {
        return Arrays.stream(array).filter(predicate).toArray();
    }

    public static void main(String[] args) {
        int[] array = new int[]{123,104,75, 81, 1, 8};
        IntPredicate predicate = x -> {
            int sum = 0;
            while (x != 0){
                sum += x % 10;
                x /= 10;
            };
            return sum % 2 == 0;
        };

        System.out.println(Arrays.toString(filter(array, x -> x % 2 == 0)));
//        System.out.println(Arrays.toString(filter(array, Sequence::evenDigitSum)));
        System.out.println(Arrays.toString(filter(array, x -> digitSum(x) % 2 == 0)));
    }

    public static int digitSum(int x){
        return x != 0 ? x % 10 + digitSum(x / 10) : 0;
    }

    public static boolean evenDigitSum(int x){
        int sum = 0;
        while (x != 0){
            sum += x % 10;
            x /= 10;
        };
        return sum % 2 == 0;
    }
}