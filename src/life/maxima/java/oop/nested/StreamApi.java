package life.maxima.java.oop.nested;

import life.maxima.java.oop.inheritance.Circle;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamApi {

    public static void main(String[] args) {
        getCircle().ifPresent(circle -> System.out.println(circle.area()));
        System.out.println("---");

        System.out.println(Arrays.toString(IntStream.rangeClosed(1, 10).toArray()));
        System.out.println(Arrays.stream(new int[]{1,2,3}).max().getAsInt());

        Random r = new Random();
        System.out.println(Arrays.toString(IntStream.generate(() -> r.nextInt(100))
                .limit(5)
                .toArray()));


        System.out.println(Arrays.toString(IntStream.rangeClosed(1, 10).filter(x -> x % 2 == 0).toArray()));
        System.out.println(IntStream.rangeClosed(1, 10).reduce(1, (a,b) -> a * b));
        System.out.println(Arrays.toString(IntStream.rangeClosed(1, 10).map(x -> x*x).toArray()));
        List<Circle> circles = IntStream.rangeClosed(1, 10)
                .mapToObj(x -> new Circle(x,0,0))
                .collect(Collectors.toList());
        System.out.println(circles);

        String result = Arrays.asList("one", "two", "three")
                .stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", "));

        System.out.println(result);


    };

    public static Optional<Circle> getCircle(){
        return Math.random()>0.5
                ? Optional.of(new Circle(5,0,0))
                : Optional.empty();
    }
}
