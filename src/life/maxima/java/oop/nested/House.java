package life.maxima.java.oop.nested;

public class House {

    private int walls;
    private int doors;
    private int windows;

    private House() {}

    public int getWalls() {
        return walls;
    }

    public int getDoors() {
        return doors;
    }

    public int getWindows() {
        return windows;
    }

    @Override
    public String toString() {
        return "House{" +
                "walls=" + walls +
                ", doors=" + doors +
                ", windows=" + windows +
                '}';
    }

    public static class Builder {
        private House house = new House();

        public Builder setWalls(int walls){
            house.walls = walls;
            return this;
        }

        public Builder setDoors(int doors){
            house.doors = doors;
            return this;
        }

        public Builder setWindows(int windows){
            house.windows = windows;
            return this;
        }

        public House build(){
            return house;
        }


    }


}
