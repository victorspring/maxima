package life.maxima.java.oop.nested;

import life.maxima.java.oop.inheritance.Circle;
import life.maxima.java.oop.inheritance.Ellipse;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamApiHomework {

    public static void main(String[] args) {
        Random random = new Random();
        List<Circle> circles = IntStream.rangeClosed(1, 10)
                .mapToObj(x -> new Circle(random.nextInt(20) + 1,0,0))
                .collect(Collectors.toList());
        System.out.println(circles);
        System.out.println(averageArea(circles));
    }

    public static double averageArea(List<Circle> circles){
        return circles.stream()
                .filter(c -> c.getR() > 10)
                .collect(Collectors.averagingDouble(Ellipse::area));

    }
}
