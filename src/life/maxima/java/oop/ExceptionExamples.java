package life.maxima.java.oop;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class ExceptionExamples {

    public static void main(String[] args) {

        System.out.println("Begin");
        try {
            checkedException();
            divideByZero();
            sof();
            indexOutOfBound();

        } catch (ArrayIndexOutOfBoundsException | ArithmeticException e) {
            e.printStackTrace(System.out);
        } catch (StackOverflowError e) {
            System.out.println("Exception caught, message: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace(System.out);
        } finally {
            System.out.println("Finally");
        }
        System.out.println("End");
    }

    public static void divideByZero() {
        System.out.println(5 / 0);
    }

    public static void sof() {
        sof();
    }

    public static void indexOutOfBound() {
        int[] array = new int[10];
        array[10] = 1;
    }

    public static void checkedException() throws IOException {
        throw new IOException();
    }

}
