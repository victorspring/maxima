1) Зарегистрироваться на gitlab.com
2) Написать мне Ваш ник(тот, под которым регистрировались на gitlab)
3) Создать на gitlab свой проект и добавить меня в участники проекта (мой ник - victorspring)
4) Создать ветку develop
5) Создать проект в Intellij IDEA, используя созданный проект в gitlab
6) Переключиться на ветку develop
7) Создать в проекте(в IDEA) файл .gitignore(скопировать содержимое из основного проекта), добавить его в Git (команда Add),
сделать коммит (команда Commit) и синхронизировать с удаленным репозиторием (команда Push)
8) Сделать merge-request на меня, чтобы мог делать ревью кода